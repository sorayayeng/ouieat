**Projet Team D - API de gestion d’un restaurant**

- **Description générale du projet**.

API dédiée à la gestion d’un restaurant basée sur l’architecture MVC. 

Requêtes possibles:

1. @GET:
   1. @GetALL : Accéder à tous les plats 
   1. @GetONE : Accéder à un plat 
   1. @GetCurrentUser: Accéder à l’utilisateur effectuant la requête
1. @POST :  
   1. Ajouter un utilisateur
   1. Ajouter un plat
1. @PATCH :  Mettre à jour un plat 
1. @DELETE : Supprimer un plat

- **Technologies & environnement**

Technologies utilisées:

[Express]: Framework de construction d’application basé sur Node.js

[Passport-js]: Middleware d’authentification pour Node.js

[MongoDB]: SGBD orienté document permettant de stocker nos données

`	`Prérequis:

`	`Pour lancer le serveur il est nécessaire de lancer la commande “npm start” ou une commande équivalente, de ce fait, il est nécessaire d’installer les commandes npm.

- **Guides d’utilisation** 

- Pour lancer le serveur:

*npm start* 

- Pour ajouter un plat

Pour ajouter un plat il vous faudra:

1. Ouvrir Postman
1. Sélectionner le type de requête “POST”
1. Utiliser l’url  [*https://localhost:3000/meals](https://localhost:3000/meals)*  
1. Renseigner le corps de la requête en suivant le modèle suivant :

{

`        `"nomDuPlat":"Poulet Braisé",

`        `"statut":"disponible",

``            `"halal":"true",

`            `"vegetarien":"false",

`           `"casher":"true",

`        `"description":"Poulet braisé issu de l'agriculture biologique",

`        `"prix":"9",

`        `"deviseDuPrix":"€",

`            `"calories":"457",

`            `"matieresGrasses":"3,1",

`            `"glucides":"2,7",

`            `"proteines":"5",

`            `"sel":"0,7"

` `}

- Pour accéder à tous les plats

Entrez l’URL suivante sur votre navigateur: [*https://localhost:3000/meals](https://localhost:3000/meals)*  

NB: Le 3000 dans l’url représente le port sur lequel le serveur tourne. Si vous souhaitez changer de port pour une raison ou une autre vous pouvez le modifier dans le dossier 

↪projet-m1-miage-2021-team-D 

`	`↪ bin

`		`↪www

- Pour accéder à un plat

Pour accéder à un plat il vous suffira de copier la chaine de caractère stockée dans la base de données dans le champs ” \_id:” puis de l’ajouter à l’url exemple: https://localhost:3000/meals/6080cee97b69971b18564940

- Ajouter un utilisateur 

Pour ajouter un utilisateur, il vous faudra :

1. Ouvrir Postman
1. Sélectionner le type de requête “POST”
1. Utiliser l’url *https://localhost[:300](https://localhost:3000/meals)0/users*
1. Renseigner le corps de la requête en suivant le modèle suivant :

{

“user” :

`	`“email” : “teamDOuiEat[@gmail.com](mailto:kaasthu@gmail.com)”,

`	`“password” : “OuiEatMdpProjet”

}

- Mettre à jour un plat

Pour supprimer un plat, il vous faudra ;

1. Ouvrir Postman
1. Sélectionner le type de requête “PATCH”
1. Utiliser l’url *https://localhost[:300](https://localhost:3000/meals)0/meals/”:id du meal”*
1. Renseignez dans le body les champs que vous souhaitez mettre à jour, inutile de remettre tout le Meal.

- Supprimer un plat

Pour supprimer un plat, il vous faudra ;

1. Ouvrir Postman
1. Sélectionner le type de requête “DELETE”
1. Utiliser l’url *https://localhost[:300](https://localhost:3000/meals)0/meals/”id du meal”*
