const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const mealSchema = new Schema(
{
    nomDuPlat:{type: String, required: true},
    statut:{type: String, required: true},

        halal:{type: Boolean, require: false},
        vegetarien:{type: Boolean, require: false},
        casher:{type: Boolean, require: false},

    description:{type: String},
    prix:{type: Number, required: false},
    deviseDuPrix:{type: String, required: false},

        calories:{type: String},
        matieresGrasses:{type: String},
        glucides:{type: String},
        proteines:{type: String},
        sel:{type: String}
},
{ collection:"Meal", timestamps: true },

);
const Meal = mongoose.model("Meal", mealSchema);
module.exports = mongoose.model("Meal", mealSchema);