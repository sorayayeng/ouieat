const mongoose = require("mongoose");
//const passportLocalMongoose = require("passeport-local-mongoose")
//const { options } = require("../routes");

const url = "mongodb://localhost:27017/mealDB";

const options =  {
    useNewUrlParser: true,
    useUnifiedTopology: true
}
mongoose.connect(url,options);


//gestion des états de la connexion avec la base
mongoose.connection.on("connecting",()=>{
    console.log("connecting");
})
mongoose.connection.on("error",()=>{
    console.log("connection error");
})
mongoose.connection.on("connected",()=>{
    console.log("connection to " + url + "successfully established" );
})
mongoose.connection.on("disconnected",()=>{
    console.log("disconnected");
})
mongoose.connection.on("reconnected",()=>{
    console.log("reconnected");
})

module.exports=mongoose.connection;