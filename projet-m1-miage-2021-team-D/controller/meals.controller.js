const { db } = require("../models/db");
const Meal = require("../models/meal.model")

let controller = {

    getAll:('/meals', async (req,res) => {
        try {
            const meals = await Meal.find()
        res.send(meals)
        }catch {
            res.status(404)
           res.send({error:"Votre restaurant n'a pas encore enregistré de plats"})
        }
    }),
    addOne:('/meals/:id', async (req,res) => {
        let meal = new Meal({
        nomDuPlat:req.body.nomDuPlat,
        statut:req.body.statut,
            halal:req.body.halal,
            vegetarien:req.body.vegetarien,
            casher:req.body.casher,

        description:req.body.description,
        prix:req.body.prix,
        deviseDuPrix:req.body.deviseDuPrix,

            calories:req.body.calories,
            matieresGrasses:req.body.matieresGrasses,
            glucides:req.body.glucides,
            proteines:req.body.proteines,
            sel:req.body.sel
            
        })
        await Meal.create(meal);
        res.send(meal);
        console.log("test");
    }),
    getOne:("/meals/:id", async (req, res) => {
       
       try{
        const meal = await Meal.findOne({ _id: req.params.id })
        console.log(meal);
        res.send(meal)
       } catch {
           res.status(404)
           res.send({error:"Le plat que vous cherchez n'existe pas"})
       }
    }),
    // updateOne:("/meals/:id", async (req, res) => {
    //     let meal = await Meal.findByIdAndUpdate({_id:req.params.id}, req.body, {
    //         new: true,
    //     }); 
    //     res.json(meal);
    //   }),
    updateOne:("/meals/:id", async (req, res) => {
        // let meal = await Meal.findByIdAndUpdate(req.params.id, req.body, {
        //   new: true,
        // }); 
        // res.json(meal);
        const meal = await Meal.findOne({ _id: req.params.id })
        
        console.log("je suis passé là")
        console.log(meal)

        const nomDuPlat=req.body.nomDuPlat;
        const statut=req.body.statut;
        const halal=req.body.halal;
        const vegetarien=req.body.vegetarien;
        const casher=req.body.casher;
        const description=req.body.description;
        const prix=req.body.prix;
        const deviseDuPrix=req.body.deviseDuPrix;
        const calories=req.body.calories;
        const matieresGrasses=req.body.matieresGrasses;
        const glucides=req.body.glucides;
        const proteines=req.body.proteines;
        const sel=req.body.sel;


        if (nomDuPlat){
            meal.nomDuPlat=nomDuPlat;
            }
        if (statut){
            meal.statut=statut;
            }
        if (halal){
            meal.halal=req.body.halal;
        }
        if (vegetarien){
             meal.vegetarien=req.body.vegetarien;
        }
        if (casher){
            meal.casher=req.body.casher;
        }
        if (description){
            meal.description=req.body.description;
        }
        if (prix){
            meal.prix=req.body.prix;
        }
        if (deviseDuPrix){
            meal.deviseDuPrix=req.body.deviseDuPrix;
        }
        if (calories){
            meal.calories=req.body.calories;
        }
        if (matieresGrasses){
            meal.matieresGrasses=req.body.matieresGrasses;
        }
        if (glucides){
            meal.glucides=req.body.glucides;
        }
        if (proteines){
            meal.roteines=req.body.proteines;
        }
        if (sel){
            meal.sel=req.body.sel;
        }
        await meal.save()
        console.log("j'ai sauvegardé le meal")
        res.json(meal)
      }),
    
    deleteOne:("/meals/:id", async (req, res) => {
        try {
            await Meal.deleteOne({ _id: req.params.id })
            res.status(204).send()
            console.log("plat supprimé")
        } catch {
            res.status(404)
            res.send({ error: "Le plat que vous essayez de supprimer n'existe pas" })
        }
    })
};

module.exports = controller;