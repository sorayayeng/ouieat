const { db } = require("../models/db");
const Users = require("../models/user.model")

let controller = {

    getCurrentUser: async(req, res, next) => {
        let id = req.payload.id;

        return Users.findById(id).then((user) => {
            if (!user)
                return res.sendStatus(400);
            return res.json({ user: user.toAuthJSON() });


        });
    },

    addOneUser: async(req, res, next) => {
        const { body: { user } } = req;

        if (!user.email) {
            return res.status(422).json({
                errors: {
                    email: 'is required',
                },
            });
        } else if (!user.password) {
            return res.status(422).json({
                errors: {
                    password: 'is required',
                },
            });
        }

        const finalUser = await Users.create(user);
        finalUser.setPassword(user.password);
        return finalUser.save()
            .then(() => res.json({ user: finalUser.toAuthJSON() }));
    }

        // var user = new User({
        //   email: req.body.email,
        //   username: req.body.username,
        //   firstname: req.body.firstname,
        //   lastname: req.body.lastname,
        //   admin: req.body.admin
        // })        
        // User.create(user);
        // res.send(user);
        //console.log("test");
    }   

    module.exports = controller;

