//require models
require("./models/db");
require("./models/user.model");
require("./models/meal.model");

// le require du middleware doit etre après tous les models
require("./middleware/authenticate.middleware");


var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
/*pour auth
var cors = require('cors');
var bodyParser = require('body-parser');
var jwt = require('_helpers/jwt');
var errorHandler = require('_helpers/error-handler');*/

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users.router');
//var booksRouter = require('./routes/books.router');
//var authRouter = require ('./routes/authenticate.router');
var mealsRouter = require('./routes/meals.router');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
/*//pour auth
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(cors());

//JWT auth
app.use(jwt());*/

app.use(require('./routes'));
app.use('/', indexRouter);
app.use('/users/', usersRouter);
//app.use('/books',booksRouter);
app.use('/meals',mealsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

