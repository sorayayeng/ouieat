const express = require("express");
const authenticate = require("./authenticate.router");
const { required } = require("./authenticate.router");
const userController = require("../controller/users.controller");
const controller = require("../controller/meals.controller");
//require("../models/meal.model")

//AUTHENTIFICATION:
//basic authentification
//JWT, jason web token
//Passport.js(module comme module express) pas bcrypt
//const authenticate = require("../controller/")
var router = express.Router();


//le all est pas obligatoire on peut s'en passer
router.all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Content-type", "text/plain");
        next();
    })
//router.get("/",athenticate.isSuperUser,controller.getAll);
router.get("/",controller.getAll); //récupérer tous les plats
router.post("/",controller.addOne); //ajouter un plat
//router.delete("/",controller.deleteAll);




router.get("/:id", authenticate.required,(req,res,next)=>{
    userController.getCurrentUser
    next();
},controller.getOne); //récupérer un plat (besoin de s'authentifier)
//router.get("/:id",controller.getOne);

router.put("/:id",controller.updateOne); //mettre àjour un plat
router.delete("/:id",controller.deleteOne); //supprimer un plat

module.exports=router;