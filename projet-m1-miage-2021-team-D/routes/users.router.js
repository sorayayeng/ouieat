const express = require("express");
const router = express.Router();
const authentification = require("./authenticate.router")

const controller = require("../controller/users.controller")

router.all("/users", (req, res, next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    next();
});

router.get("/getCurrentUser", authentification.required, (req, res, next) => {
    next();
}, controller.getCurrentUser);

router.post("/",controller.addOneUser);

module.exports = router;

// router
//   .route("/login")
//   .post(passport.authenticate("user", { session: false }), controller.login);

// router.route("/register").post(signupValidator, controller.register);

// router.route("/meals")
//   .get(authenticate.isAuthenticated, authenticate.isUser)
//   .post(authenticate.isAuthenticated, authenticate.isUser)

// router.route("/users/:id")
//   .patch(authenticate.isAuthenticated, authenticate.isUser)
//   .delete(authenticate.isAuthenticated, authenticate.isUser)
  
// module.exports = router;